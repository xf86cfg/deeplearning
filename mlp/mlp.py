import numpy, uuid, scipy.special, math, pickle
import mlp.models
import json

class MLP:
    
    def __init__(self, inputnodes = None, hiddennodes = None, outputnodes = None, learningrate = None, identifier = None):
        self._modelObject = None
        #activation function
        self.activation_function = lambda x: scipy.special.expit(x)
        self.inverse_activation_function = lambda x: scipy.special.logit(x)
        
        if identifier:
            self._loadWithIdentifier(identifier)
        else:
            self.guid = uuid.uuid1().hex
            self._nist_epoch = 0
            self._user_epoch = 0
            self._user_iterations = 0

            #set number of nodes in each layer
            self.inodes = inputnodes
            self.hnodes = hiddennodes
            self.onodes = outputnodes
        
            #learning rate
            self.lr = learningrate
            #initialize weights
            self.wih = numpy.random.normal(0.0, (1.0/math.sqrt(self.hnodes)), (self.hnodes, self.inodes))
            self.who = numpy.random.normal(0.0, (1.0/math.sqrt(self.onodes)), (self.onodes, self.hnodes))
            
            self.save()

    def _loadWithIdentifier(self, identifier):
        try:
            self._modelObject = mlp.models.MLPModel.objects.get(guid = identifier)
            self.guid = self._modelObject.guid
            self.inodes = self._modelObject.inputnodes
            self.hnodes = self._modelObject.hiddennodes
            self.onodes = self._modelObject.outputnodes
            self.lr = self._modelObject.learningrate
            self._nist_epoch = self._modelObject.nist_epoch
            self._user_epoch = self._modelObject.user_epoch
            self._user_iterations = self._modelObject.user_iterations
            self.wih = pickle.loads(self._modelObject.wih)
            self.who = pickle.loads(self._modelObject.who)

        except Exception:
            raise Exception("Cannot retrieve network with requested identifier")

    def train(self, input_list, target_list):
        #convert input lists into 2d array
        inputs = numpy.array(input_list, ndmin=2).T
        targets = numpy.array(target_list, ndmin=2).T
        
        #calulate signals into hidden layer
        hidden_inputs = numpy.dot(self.wih, inputs)
        
        #calulate signals from hidden layer
        hidden_outputs = self.activation_function(hidden_inputs)
        
        #calculate signals into final output layer
        final_inputs = numpy.dot(self.who, hidden_outputs)
        
        #calculate signals from final output layer
        final_outputs = self.activation_function(final_inputs)
        
        #output error is (target - output)
        output_errors = targets - final_outputs
        
        #hidden error is
        hidden_errors = numpy.dot(self.who.T, output_errors)
        
        #update the weights for the links between the hidden and output layers
        self.who += self.lr * numpy.dot((output_errors * final_outputs * (1.0 - final_outputs)), numpy.transpose(hidden_outputs))

        #update the weights for the links between the input and hidden layers
        self.wih += self.lr * numpy.dot((hidden_errors * hidden_outputs * (1.0 - hidden_outputs)), numpy.transpose(inputs))
    
    def query(self, input_list):
        #convert input list into 2d array
        inputs = numpy.array(input_list, ndmin=2).T
        
        #calulate signals into hidden layer
        hidden_inputs = numpy.dot(self.wih, inputs)
        
        #calulate signals from hidden layer
        hidden_outputs = self.activation_function(hidden_inputs)
        
        #calculate signals into final output layer
        final_inputs = numpy.dot(self.who, hidden_outputs)
        
        #calculate signals from final output layer
        final_outputs = self.activation_function(final_inputs)
        
        return final_outputs
    

    def backquery(self, targets_list):
        # transpose the targets list to a vertical array
        final_outputs = numpy.array(targets_list, ndmin=2).T
        
        # calculate the signal into the final output layer
        final_inputs = self.inverse_activation_function(final_outputs)

        # calculate the signal out of the hidden layer
        hidden_outputs = numpy.dot(self.who.T, final_inputs)
        # scale them back to 0.01 to .99
        hidden_outputs -= numpy.min(hidden_outputs)
        hidden_outputs /= numpy.max(hidden_outputs)
        hidden_outputs *= 0.98
        hidden_outputs += 0.01
        
        # calculate the signal into the hideen layer
        hidden_inputs = self.inverse_activation_function(hidden_outputs)
        
        # calculate the signal out of the input layer
        inputs = numpy.dot(self.wih.T, hidden_inputs)
        # scale them back to 0.01 to .99
        inputs -= numpy.min(inputs)
        inputs /= numpy.max(inputs)
        inputs *= 0.98
        inputs += 0.01
        
        return inputs

    def save(self):
        if not self._modelObject:
            self._modelObject, created = mlp.models.MLPModel.objects.get_or_create(guid = self.guid)
            #If new object then save all
            if created:
                self._modelObject.guid = self.guid
                self._modelObject.inputnodes = self.inodes
                self._modelObject.hiddennodes = self.hnodes
                self._modelObject.outputnodes = self.onodes
                self._modelObject.learningrate = self.lr

        #If existing object then save just changed data
        self._modelObject.nist_epoch = self._nist_epoch
        self._modelObject.user_epoch = self._user_epoch
        self._modelObject.user_iterations = self._user_iterations
        self._modelObject.wih = pickle.dumps(self.wih)
        self._modelObject.who = pickle.dumps(self.who)
        
        self._modelObject.save()

    def toJSON(self):
        data_verbose = { "guid": self.guid, "input_nodes": self.inodes, "hidden_nodes": self.hnodes, "output_nodes": self.onodes,
            "learning_rate": self.lr, "wih": self.wih.tolist(), "who": self.who.tolist(),
            "nist_epoch": self._nist_epoch, "user_epoch": self._user_epoch, "user_iterations": self._user_iterations }
        data = { "guid": self.guid, "input_nodes": self.inodes, "hidden_nodes": self.hnodes, "output_nodes": self.onodes,
            "learning_rate": self.lr, "nist_epoch": self._nist_epoch, "user_epoch": self._user_epoch, "user_iterations": self._user_iterations }
        return json.dumps(data)
