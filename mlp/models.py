from django.db import models

# Create your models here.

class MLPModel(models.Model):
    guid = models.CharField(max_length=32)
    nist_epoch = models.IntegerField(null=False, default=0)
    user_epoch = models.IntegerField(null=False, default=0)
    user_iterations = models.IntegerField(null=False, default=0)
    inputnodes = models.IntegerField(null=False, default=0)
    hiddennodes = models.IntegerField(null=False, default=0)
    outputnodes = models.IntegerField(null=False, default=0)
    learningrate = models.FloatField(null=False, default=0)
    wih = models.BinaryField(null=False)
    who = models.BinaryField(null=False)