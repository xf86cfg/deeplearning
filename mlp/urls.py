from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^api/create_network$', views.create_network, name='create_network'),
    url(r'^api/retrieve_network$', views.retrieve_network, name='retrieve_network'),
    url(r'^api/train_network$', views.train_network, name='train_network'),
    url(r'^api/train_network_nist_set$', views.train_network_nist_set, name='train_network_nist_set'),
    url(r'^api/train_network_user_set$', views.train_network_nist_set, name='train_network_user_set'),
    url(r'^api/query_network$', views.query_network, name='query_network'),
    url(r'^api/backquery_network$', views.backquery_network, name='backquery_network'),
]