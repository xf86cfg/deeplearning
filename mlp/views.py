from django.shortcuts import render
from django.http import HttpResponse, HttpResponseBadRequest, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from mlp.mlp import MLP
import pdb, json

# Create your views here.

def index(request):
    network = MLP(identifier = "d2460f869f5011e7b2e70088653646fc")
    print("network id:")
    print(network.guid)

    return HttpResponse("Hello, world. You're at the mlp index.")

@csrf_exempt
def create_network(request):
    if request.method == 'POST' and request.content_type == 'application/json':
        try:
            json_data = json.loads(request.body.decode("utf-8"))
            hidden_nodes = int(json_data['hidden_nodes'])
            learning_rate = float(json_data['learning_rate'])
            input_nodes = 784
            output_nodes = 10
            network = MLP(input_nodes, hidden_nodes, output_nodes, learning_rate)
            network.save()
            return HttpResponse(status=201, content=network.guid)
        except Exception as e:
            return HttpResponseBadRequest()
    else:
        return HttpResponseBadRequest()

def retrieve_network(request):
    if request.method == 'GET':
        try:
            guid = request.GET['guid']
            network = MLP(identifier = guid)
            data_json = network.toJSON()
            return JsonResponse(data_json, safe=False)
        except Exception as e:
            print(e)
            return HttpResponseBadRequest()
        pass
    else:
        return HttpResponseBadRequest()

def train_network_nist_set(request):
    pass

def train_network_user_set(request):
    pass

def train_network(request):
    pass

def query_network(request):
    pass

def backquery_network(request):
    pass




